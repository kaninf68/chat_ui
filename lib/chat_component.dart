library chat_component;

import 'package:chat_component/model/string.dart';
import 'package:chat_component/model/struct/state.dart';
import 'package:flutter/material.dart';

import 'package:chat_component/model/struct/chat_message_data.dart';
import 'package:chat_component/model/style.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'model/time.dart';
import 'widget/avatar.dart';
import 'widget/camera_button.dart';
import 'widget/dashed_box_item.dart';
import 'widget/gallery_button.dart';
import 'widget/image_box.dart';
import 'widget/message_box.dart';

class ChatComponent extends StatefulWidget {
  final int ownerId;
  final int peerId;
  final String peerAvatar;
  final Function onSentMessage;
  final bool disabledInput;
  final List messages;
  final String prefixImageUrl;
  final FocusNode focusNode;
  ChatComponent(
      {@required this.ownerId,
      @required this.peerAvatar,
      @required this.peerId,
      @required this.messages,
      @required this.onSentMessage,
      this.disabledInput,
      this.prefixImageUrl,
      @required this.focusNode});

  @override
  _ChatComponentState createState() => _ChatComponentState();
}

class _ChatComponentState extends State<ChatComponent>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;

  int _ownerId;
  // int _peerId;
  String _peerAvatar;

  bool _disableInput;

  List<ChatMessageData> listMessage = [];

  final TextEditingController textEditingController = TextEditingController();
  final ScrollController listScrollController = ScrollController();
  FocusNode focusNode;

  FToast fToast;
  AnimationController _animationController;

  _scrollListener() {
    if (listScrollController.offset >=
            listScrollController.position.maxScrollExtent &&
        !listScrollController.position.outOfRange) {
      // print("reach the bottom");
      setState(() {
        // print("reach the bottom");
        // _limit += _limitIncrement;
      });
    }
    if (listScrollController.offset <=
            listScrollController.position.minScrollExtent &&
        !listScrollController.position.outOfRange) {
      // print("reach the top");
      setState(() {
        // print("reach the top");
      });
    }
  }

  @override
  void initState() {
    super.initState();
    // listScrollController.addListener(_scrollListener);
    _ownerId = widget.ownerId;
    // _peerId = widget.peerId;
    _peerAvatar = widget.peerAvatar;
    _disableInput = widget.disabledInput == true;
    listMessage = ChatMessageDataList().parse(widget.messages);
    focusNode = widget.focusNode;

    //
    fToast = FToast();
    fToast.init(context);
    //
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100),
    );
  }

  @override
  void deactivate() {
    super.deactivate();
    unFocus();
  }

  @override
  dispose() {
    super.dispose();
    //update home notification
    // unFocus();
    textEditingController?.dispose();
  }

  clearListMessage() {
    listMessage.clear();
  }

  unFocus() {
    if (focusNode.hasFocus) {
      focusNode.unfocus();
    }
  }

  Widget buildItem(int index, ChatMessageData data) {
    EdgeInsetsGeometry _marginForRightMessage = EdgeInsets.only(
        bottom: 10.0); // isLastMessageRight(index) ? 10.0 : 10.0

    EdgeInsetsGeometry _marginForLeftMessage = EdgeInsets.only(
      left: 10.0,
      bottom: 10.0, // isLastMessageLeft(index) ? 10.0 : 10.0
    );

    EdgeInsetsGeometry _margin;
    CrossAxisAlignment _crossAxisAlignment;
    MainAxisAlignment _mainAxisAlignment;

    Color _textColor;
    Color _boxColor;

    if (data.senderId == _ownerId) {
      // right message
      _margin = _marginForRightMessage;
      _crossAxisAlignment = CrossAxisAlignment.center;
      _mainAxisAlignment = MainAxisAlignment.end;
      _textColor = UStyle.GrayColor19;
      _boxColor = UStyle.GrayColor18;
    } else {
      _margin = _marginForLeftMessage;
      _crossAxisAlignment = CrossAxisAlignment.start;
      _mainAxisAlignment = MainAxisAlignment.start;
      _textColor = UStyle.GrayColor17;
      _boxColor = UStyle.GrayColor16;
    }
    bool _showTimestamp = false;

    if (index == listMessage.length - 1) {
      _showTimestamp = true;
    } else {
      if (listMessage[index].timestamp.day !=
          listMessage[index + 1].timestamp.day) {
        _showTimestamp = true;
      } else {
        _showTimestamp = false;
      }
    }

    return Column(
      children: [
        _showTimestamp
            ? Container(
                child: Text(
                  UString.dateTimeStamp(data.timestamp),
                  style: TextStyle(color: UStyle.GrayColor17, fontSize: 12.0),
                ),
                padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
                constraints: BoxConstraints(maxWidth: 200.0),
                decoration: BoxDecoration(
                    color: _boxColor,
                    borderRadius: BorderRadius.all(Radius.circular(24.0))),
                margin: EdgeInsets.only(top: 15.0, bottom: 10.0),
              )
            : SizedBox(),
        Row(
          crossAxisAlignment: _crossAxisAlignment,
          mainAxisAlignment: _mainAxisAlignment,
          children: <Widget>[
            // avatar
            _buildAvatar(data.senderId != _ownerId, _peerAvatar,
                isLastMessageLeft(index)),
            // image
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                data.senderId == _ownerId
                    ? Container(
                        margin: EdgeInsets.only(right: 5.0, bottom: 10.0),
                        child: Text(
                          UTime.timeStamp(data.timestamp),
                          style: TextStyle(
                            color: UStyle.GrayColor17,
                            fontSize: 10.0,
                            // fontStyle: FontStyle.italic,
                          ),
                        ),
                      )
                    : SizedBox(),
                Column(
                  children: [
                    ImageBox(
                      image: data.image,
                      margin: _margin,
                      prefixImageUrl: widget.prefixImageUrl,
                    ),
                    MessageBox(
                      data.content,
                      textColor: _textColor,
                      boxColor: _boxColor,
                      isLeftSize: data.senderId != _ownerId,
                      margin: _margin,
                      position: checkPositionMessageBox(index),
                    ),
                  ],
                ),
                data.senderId != _ownerId
                    ? Container(
                        margin: EdgeInsets.only(left: 5.0, bottom: 10.0),
                        child: Text(
                          UTime.timeStamp(data.timestamp),
                          style: TextStyle(
                            color: UStyle.GrayColor17,
                            fontSize: 10.0,
                            // fontStyle: FontStyle.italic,
                          ),
                        ),
                      )
                    : SizedBox(),
              ],
            ),
            // message
          ],
        ),
      ],
    );
  }

  String checkPositionMessageBox(int index) {
    Map _listMessageMap = listMessage.asMap();
    String _previous = hasPreviousMessageLeft(_listMessageMap, index);
    String _after = hasAfterMessageLeft(_listMessageMap, index);
    // print(_previous + _after);

    switch (_previous + _after) {
      case '11':
        return MessagePosition.MESSAGE_POSITION_MIDDLE;
        break;
      case '10':
        return MessagePosition.MESSAGE_POSITION_BOTTOM;
        break;
      case '01':
        return MessagePosition.MESSAGE_POSITION_TOP;
        break;
      case '00':
        return MessagePosition.MESSAGE_POSITION_ALONE;
        break;
      default:
        return MessagePosition.MESSAGE_POSITION_ALONE;
    }
  }

  String hasPreviousMessageLeft(Map m, int index) {
    if (m[index - 1] != null) {
      if (m[index - 1].senderId == m[index].senderId) {
        return '1';
      } else {
        return '0';
      }
    } else {
      return '0';
    }
  }

  String hasAfterMessageLeft(Map m, int index) {
    if (m[index + 1] != null) {
      if (m[index + 1].senderId == m[index].senderId) {
        return '1';
      } else {
        return '0';
      }
    } else {
      return '0';
    }
  }

  bool isLastMessageLeft(int index) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[index - 1].senderId == _ownerId) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[index - 1].senderId != _ownerId) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  Widget _buildAvatar(bool wantAvatar, String avatar, bool isLast) {
    if (!wantAvatar) {
      return SizedBox();
    }
    return isLast
        ? Avatar(
            _peerAvatar ?? '',
            radius: 20,
            borderColor: UStyle.OrangeColor2,
          )
        : SizedBox(width: 40.0);
  }

  Widget buildInput() {
    return Container(
      width: double.infinity,
      // height: 60.0,
      constraints: BoxConstraints(maxHeight: 120.0),
      decoration: BoxDecoration(
        // border: Border(top: BorderSide(width: 0.5)),
        color: UStyle.AppbarColor,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          // Button image
          CameraButton(
            color: UStyle.WhiteColor1,
            onSentImage: (String image) {
              onSendMessage(image, 1);
            },
          ),
          // Button send image
          GalleryButton(
            color: UStyle.WhiteColor1,
            onSentImage: (String image) {
              onSendMessage(image, 1);
            },
          ),

          // Edit text
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(24.0),
                color: UStyle.OrangeColor5,
              ),
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5.0),
              padding: EdgeInsets.only(left: 15),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Flexible(
                    child: Container(
                      padding: EdgeInsets.only(bottom: 9.0),
                      child: TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        textInputAction: TextInputAction.newline,
                        style: TextStyle(fontSize: 15.0),
                        controller: textEditingController,
                        decoration: InputDecoration.collapsed(
                          hintStyle: TextStyle(color: UStyle.GrayColor6),
                          hintText: 'พิมพ์ข้อความ',
                          // hintStyle: TextStyle(color: UStyle.GrayColor6),
                        ),
                        focusNode: focusNode,
                        onChanged: (d) {
                          if (textEditingController.text.isNotEmpty &&
                              textEditingController.text.trim() != '') {
                            _animationController.forward();
                          } else {
                            _animationController.reverse();
                          }
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 56.0,
                    height: 40.0,
                    child: SlideTransition(
                      position: Tween<Offset>(
                        begin: Offset(0.3, 0.0),
                        end: Offset.zero,
                      ).animate(_animationController),
                      child: FadeTransition(
                          opacity: _animationController,
                          child: IconButton(
                            icon: Icon(Icons.send_rounded),
                            padding: EdgeInsets.all(0.0),
                            color: UStyle.AppbarColor,
                            onPressed: () =>
                                onSendMessage(textEditingController.text, 0),
                            // color: UStyle.BlueColor1,
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),

          // Button send message
        ],
      ),
    );
  }

  void onSendMessage(String content, int type) {
    // type: 0 = text, 1 = image, 2 = sticker
    if (type == 0) {
      if (content.trim() != '') {
        textEditingController.clear();
        widget.onSentMessage(content.trim(), 0);

        if (content != '') {
          ChatMessageData newMessage = ChatMessageData({
            'userId': widget.ownerId,
            'message': content.trim(),
            'createdAt': UTime.dateToString(
              DateTime.now(),
              DateTime.now().hour.toString(),
              DateTime.now().minute.toString(),
              DateTime.now().second.toString(),
            )
          });
          listMessage.insert(0, newMessage);
          setState(() {});
        }
        _animationController.reverse();
        listScrollController.animateTo(0.0,
            duration: Duration(milliseconds: 300), curve: Curves.easeOut);
      } else {
        // fToast.removeCustomToast();
        // _showToast();
      }
    } else {
      widget.onSentMessage(content, 1);
      listScrollController.animateTo(0.0,
          duration: Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  // _showToast() {
  //   Widget toast = Container(
  //     padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
  //     decoration: BoxDecoration(
  //       borderRadius: BorderRadius.circular(25.0),
  //       color: UStyle.GrayColor15,
  //     ),
  //     child: Text("ไม่มีข้อความส่ง"),
  //   );

  //   fToast.showToast(
  //     child: toast,
  //     gravity: ToastGravity.BOTTOM,
  //     toastDuration: Duration(seconds: 2),
  //   );
  // }

  Future<bool> onBackPress() {
    unFocus();

    return Future.value(true);
  }

  @override
  void didUpdateWidget(covariant ChatComponent oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.ownerId != oldWidget.ownerId) {
      _ownerId = widget.ownerId;
    }

    setState(() {
      print('didUpdateWidget');
      listMessage = ChatMessageDataList().parse(widget.messages);
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // print('listMessage build ' + listMessage.first.content);
    // print('listMessage build ' + listMessage.length.toString());
    return WillPopScope(
        onWillPop: onBackPress,
        child: Scaffold(
          body: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  // List of messages
                  Flexible(
                    child: listMessage.length != 0
                        ? ListView.builder(
                            keyboardDismissBehavior:
                                ScrollViewKeyboardDismissBehavior.onDrag,
                            padding: EdgeInsets.all(10.0),
                            itemBuilder: (context, index) =>
                                buildItem(index, listMessage[index]),
                            itemCount: listMessage.length,
                            reverse: true,
                            controller: listScrollController,
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              DashedBoxItem(
                                Icons.message_outlined,
                                paddingLeftNRight: 50.0,
                              ),
                            ],
                          ),
                  ),

                  // Input content
                  _disableInput ? SizedBox() : buildInput(),
                ],
              ),

              // Loading
              // buildLoading()
            ],
          ),
        ));
  }
}
