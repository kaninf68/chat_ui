import 'package:cached_network_image/cached_network_image.dart';
import 'package:chat_component/model/image.dart';
import 'package:chat_component/model/style.dart';
import 'package:chat_component/pages/photo_page.dart';
import 'package:flutter/material.dart';

class ImageItem extends StatelessWidget {
  final String image;
  final double width;
  final BuildContext context;
  ImageItem(this.image, this.width, this.context);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
        child: Material(
          child: CachedNetworkImage(
            placeholder: (context, url) => Container(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(UStyle.GrayColor6),
              ),
              width: width,
              height: width,
              padding: EdgeInsets.all(70.0),
              decoration: BoxDecoration(
                color: UStyle.GrayColor12,
                borderRadius: BorderRadius.all(
                  Radius.circular(4.0),
                ),
              ),
            ),
            errorWidget: (context, url, error) => Container(
              child: Image.network(
                UImage.BROKEN_IMAGE,
                width: width,
                height: width,
                fit: BoxFit.cover,
              ),
              decoration: BoxDecoration(
                // border: Border.all(color: UStyle.GrayColor7),
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
                color: UStyle.GrayColor18,
              ),
              clipBehavior: Clip.hardEdge,
            ),
            imageUrl: image,
            width: width,
            height: width,
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.all(Radius.circular(4.0)),
          clipBehavior: Clip.hardEdge,
        ),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => PhotoPage(image)));
        },
        padding: EdgeInsets.all(0),
      ),
      // margin: margin,
    );
  }
}
