import 'dart:io';

import 'package:chat_component/model/image.dart';
import 'package:chat_component/model/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class GalleryButton extends StatefulWidget {
  final Function onSentImage;
  final Color color;

  GalleryButton({@required this.onSentImage, this.color});

  @override
  _GalleryButtonState createState() => _GalleryButtonState();
}

class _GalleryButtonState extends State<GalleryButton> {
  List<Asset> images = List<Asset>();

  Future<void> loadAssets() async {
    setState(() {
      images = List<Asset>();
    });

    List<Asset> resultList;
    String error;

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        materialOptions: MaterialOptions(
            actionBarTitle: 'อัลบัม',
            actionBarColor: '#F28530',
            statusBarColor: '#000000'),
      );
      String _m = '';

      for (var asset in resultList) {
        ByteData _name = await asset.getByteData();
        File file = await UImage.writeToFile(_name);
        String imagePath = await UImage.compressAndSaveFile('gallery', file);
        _m += imagePath + ',';
      }

      _m = _m.substring(0, _m.length - 1);
      widget.onSentImage(_m);
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      if (error == null) print('No Error Dectected');
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 40,
      child: IconButton(
        iconSize: 24,
        splashRadius: 8.0,
        padding: EdgeInsets.all(2.0),
        icon: Icon(
          Icons.panorama,
        ),
        color: widget.color ?? UStyle.BlackColor0,
        onPressed: () {
          loadAssets();
          // print('camera_alt');
        },
        // color: UStyle.BlueColor1,
      ),
    );
  }
}
