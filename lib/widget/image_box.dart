import 'package:chat_component/widget/image_item.dart';
import 'package:flutter/material.dart';

class ImageBox extends StatelessWidget {
  final String image;
  final String prefixImageUrl;
  final EdgeInsetsGeometry margin;
  ImageBox({@required this.image, this.prefixImageUrl, this.margin});

  List<String> _createImageList() {
    List<String> imageList = image.split(',');
    List<String> _image = [];
    imageList.forEach((element) {
      _image.add(prefixImageUrl + element);
    });

    return _image;
  }

  @override
  Widget build(BuildContext context) {
    if (image == null || image == '') {
      return SizedBox();
    }
    List<String> _image = _createImageList();
    return Container(
      margin: margin,
      child: _image.length > 1
          ? Container(
              width: 205,
              child: GridView.count(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 2,
                children: List.generate(
                  _image.length,
                  (index) {
                    return ImageItem(_image[index], 100, context);
                  },
                ),
              ),
            )
          : ImageItem(_image[0], 150, context),
    );
  }
}
