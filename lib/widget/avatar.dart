import 'package:chat_component/model/image.dart';
import 'package:flutter/material.dart';

import 'package:chat_component/model/style.dart';
// import 'package:ttrux/model/style.dart';

class Avatar extends StatelessWidget {
  final String image;
  final double radius;
  final Color borderColor;

  Avatar(this.image, {this.radius, this.borderColor});

  @override
  Widget build(BuildContext context) {
    double size = radius != null ? radius * 2 : 64.0;
    String _image = image != '' ? image : '';
    return Container(
      padding: EdgeInsets.all(3.0),
      height: size,
      width: size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: borderColor ?? UStyle.WhiteColor2,
      ),
      child: CircleAvatar(
        backgroundColor: Colors.white,
        radius: radius ?? 32.0,
        backgroundImage:
            UImage.getImage(_image, dummy: UImage.USER_DUMMY_IMAGE),
      ),
    );
  }
}

//image == 'avatar/'
// ? Icon(
//     Icons.account_circle,
//     size: size - 6.0,
//     color: UStyle.GrayColor1,
//   )
// : CircleAvatar(
//     backgroundColor: UStyle.GrayColor14,
//     radius: radius ?? 32.0,
//     backgroundImage:
//         UImage.getImage(image, dummy: UImage.USER_DUMMY_IMAGE),
//   )
