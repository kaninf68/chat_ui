import 'package:dashed_container/dashed_container.dart';

import 'package:flutter/material.dart';

import 'package:chat_component/model/style.dart';

class DashedBoxItem extends StatelessWidget {
  final IconData icon;
  final Function onTapHandler;
  final double paddingTop;
  final double paddingBottom;
  final double paddingLeftNRight;

  DashedBoxItem(
    this.icon, {
    this.onTapHandler,
    this.paddingTop,
    this.paddingBottom,
    this.paddingLeftNRight,
  });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: paddingTop ?? 0.0,
          bottom: paddingBottom ?? 0.0,
          left: paddingLeftNRight ?? 0.0,
          right: paddingLeftNRight ?? 0.0),
      child: GestureDetector(
        onTap: onTapHandler ?? () {},
        child: DashedContainer(
          child: Container(
            height: 100.0,
            width: double.infinity,
            decoration: BoxDecoration(
                color: UStyle.GrayColor9,
                borderRadius: BorderRadius.circular(10.0)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  icon,
                  size: 40.0,
                  color: UStyle.GrayColor5,
                ),
                Text(
                  'ยังไม่มีข้อความ',
                  style: TextStyle(fontSize: 20.0, color: UStyle.GrayColor2),
                ),
              ],
            ),
          ),
          dashColor: UStyle.GrayColor2,
          borderRadius: 10.0,
          dashedLength: 15.0,
          blankLength: 5.0,
          strokeWidth: 3.0,
        ),
      ),
    );
  }
}
