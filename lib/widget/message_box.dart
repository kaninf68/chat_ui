import 'package:flutter/material.dart';

import 'package:chat_component/model/struct/state.dart';
import 'package:chat_component/model/style.dart';

class MessageBox extends StatefulWidget {
  final String content;
  final Color textColor;
  final Color boxColor;
  final bool isLeftSize;
  final EdgeInsetsGeometry margin;
  final String position;

  MessageBox(
    this.content, {
    this.textColor,
    this.boxColor,
    this.isLeftSize,
    this.margin,
    this.position,
  });

  @override
  _MessageBoxState createState() => _MessageBoxState();
}

class _MessageBoxState extends State<MessageBox> {
  String _content;
  String _position;
  Color _textColor;
  Color _boxColor;
  EdgeInsetsGeometry _margin;
  bool _isLeftSize;

  @override
  void initState() {
    super.initState();
    _content = widget.content;
    _textColor = widget.textColor ?? UStyle.BlackColor0;
    _boxColor = widget.boxColor ?? UStyle.GrayColor14;
    _margin = widget.margin ?? EdgeInsets.all(8.0);
    _isLeftSize = widget.isLeftSize == true;
    _position = widget.position;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant MessageBox oldWidget) {
    super.didUpdateWidget(oldWidget);
    // print(widget.content + ' ' + oldWidget.content);
    // if (widget.content != oldWidget.content) {
    setState(() {
      _content = widget.content;
      _textColor = widget.textColor;
      _boxColor = widget.boxColor;
      _margin = widget.margin;
      _position = widget.position;
      _isLeftSize = widget.isLeftSize;
    });
    // }
  }

  BorderRadiusGeometry _getBorderRadius(_position) {
    double _radiusValue = 24.0;
    Radius _radius = Radius.circular(_radiusValue);

    double _radiusABitValue = 4.0;
    Radius _radiusABit = Radius.circular(_radiusABitValue);
    switch (_position) {
      case MessagePosition.MESSAGE_POSITION_MIDDLE:
        if (!_isLeftSize) {
          return BorderRadius.only(
              topLeft: _radius,
              bottomLeft: _radius,
              topRight: _radiusABit,
              bottomRight: _radiusABit);
        } else {
          return BorderRadius.only(
              topRight: _radius,
              bottomRight: _radius,
              topLeft: _radiusABit,
              bottomLeft: _radiusABit);
        }

        break;
      case MessagePosition.MESSAGE_POSITION_BOTTOM:
        if (_isLeftSize) {
          return BorderRadius.only(
            topLeft: _radius,
            bottomLeft: _radiusABit,
            topRight: _radius,
            bottomRight: _radius,
          );
        } else {
          return BorderRadius.only(
            topLeft: _radius,
            bottomLeft: _radius,
            topRight: _radius,
            bottomRight: _radiusABit,
          );
        }

        break;
      case MessagePosition.MESSAGE_POSITION_TOP:
        if (_isLeftSize) {
          return BorderRadius.only(
            topLeft: _radiusABit,
            bottomLeft: _radius,
            topRight: _radius,
            bottomRight: _radius,
          );
        } else {
          return BorderRadius.only(
            topLeft: _radius,
            bottomLeft: _radius,
            topRight: _radiusABit,
            bottomRight: _radius,
          );
        }

        break;
      case MessagePosition.MESSAGE_POSITION_ALONE:
        return BorderRadius.circular(_radiusValue);
        break;
      default:
        return BorderRadius.circular(_radiusValue);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.content == null || widget.content == '') {
      return SizedBox();
    }

    return Column(
      crossAxisAlignment:
          _isLeftSize ? CrossAxisAlignment.start : CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: [
        // GestureDetector(
        //   onTap: () {
        //     setState(() {
        //       _isShowTimeStamp = !_isShowTimeStamp;
        //       if (_isShowTimeStamp) {
        //         EdgeInsetsGeometry _m =
        //             _margin.subtract(EdgeInsets.only(bottom: 8.0));
        //         _margin = _m.isNonNegative ? _m : widget.margin;
        //       } else {
        //         _margin = widget.margin;
        //       }
        //     });
        //   },
        //   child:
        Container(
          child: Text(
            _content,
            style: TextStyle(color: _textColor),
          ),
          padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
          constraints: BoxConstraints(maxWidth: 200.0),
          decoration: BoxDecoration(
              color: _boxColor, borderRadius: _getBorderRadius(_position)),
          margin: _margin,
        )
        // ),
        // _isShowTimeStamp
        //     ? TText(L.get('เห็นแล้ว'),
        //         style: TextStyle(
        //             color: UStyle.GrayColor15,
        //             fontSize: 12.0,
        //             fontStyle: FontStyle.italic))
        //     : SizedBox(),
      ],
    );
  }
}
