import 'dart:io';

import 'package:chat_component/model/image.dart';
import 'package:chat_component/model/style.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CameraButton extends StatelessWidget {
  final Function onSentImage;
  final Color color;

  CameraButton({@required this.onSentImage, this.color});
  _cameraHadler(ImageSource source) {
    _onImageButtonPressed(source).then((path) {
      if (path != null) {
        imageCache.clear();
      }
    });
  }

  Future<String> _onImageButtonPressed(ImageSource source) async {
    // double _screenHeight = (_screenWidth / 16) * 9;

    final _picker = ImagePicker();
    String imagePath;

    PickedFile pickedFile = await _picker.getImage(
      source: source,
      imageQuality: 30,
      // maxWidth: _screenWidth,
      // maxHeight: _screenHeight
    );
    if (pickedFile == null) {
      return null;
    }
    File onValue = File(pickedFile.path);
    if (onValue != null) {
      imagePath = await UImage.compressAndSaveFile('image', onValue);
      onSentImage(imagePath);
      imageCache.clear();
      if (source == ImageSource.camera) {
        onValue.delete();
      }
    }
    return Future.value(imagePath);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 40,
      child: IconButton(
        iconSize: 24.0,
        splashRadius: 8.0,
        padding: EdgeInsets.all(2.0),
        icon: Icon(Icons.camera_alt),
        color: color ?? UStyle.BlackColor0,
        onPressed: () {
          // print('camera_alt');
          _cameraHadler(ImageSource.camera);
        },
        // color: UStyle.BlueColor1,
      ),
    );
  }
}
