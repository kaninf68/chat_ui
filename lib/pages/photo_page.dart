import 'dart:io';
import 'dart:typed_data';
//import 'dart:typed_data';

import 'package:chat_component/model/image.dart';
import 'package:chat_component/model/style.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class PhotoPage extends StatelessWidget {
  final image;
  PhotoPage(this.image);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UStyle.BlackColor0,
      appBar: AppBar(
        backgroundColor: UStyle.AppbarColor,
        title: Text('รูปภาพขนาดจริง'),
      ),
      body: Center(
          child: PhotoView(
        backgroundDecoration: BoxDecoration(color: UStyle.BlackColor0),
        minScale: PhotoViewComputedScale.contained,
        maxScale: PhotoViewComputedScale.contained * 4.0,
        initialScale: PhotoViewComputedScale.contained,
        imageProvider: image is Uint8List
            ? MemoryImage(image)
            : (image.indexOf('http') == 0)
                ? UImage.getImage(image)
                : Image.file(File(image)).image,
      )

          // Image(
          //   image: image is Uint8List
          //       ? MemoryImage(image)
          //       : (image.indexOf('http') == 0)
          //           ? UImage.getImage(image)
          //           : Image.file(File(image)).image,
          // ),
          ),
    );
  }
}
