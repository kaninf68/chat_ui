import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;

import 'package:flutter_image/network.dart';
import 'package:path_provider/path_provider.dart';

class UImage {
  //images
  static const String BROKEN_IMAGE =
      'http://www.ttrux.com/images/broken-image.png';
  static const String USER_DUMMY_IMAGE =
      'http://www.ttrux.com/images/user-icon.png';

  static const String NO_IMAGE = 'assets/images/img_not_available.jpeg';

  static String imagePathToBast64(String path) {
    File file = File(path);
    Uint8List bytes = file.readAsBytesSync();
    return base64Encode(bytes);
  }

  static NetworkImageWithRetry getImage(String image, {String dummy}) {
    if (image == '' || image == null) {
      if (dummy != null) {
        return getImage(dummy);
      }
      return null;
    }

    //check extendsion
    if (image.indexOf('.png') == -1 && image.indexOf('.jpg') == -1) {
      if (dummy != null) {
        return getImage(dummy);
      }
      return null;
    }

    String url;
    if (image.indexOf('http') == -1) {
      url = image;
    } else {
      url = image;
    }

    NetworkImageWithRetry data = NetworkImageWithRetry(url,
        fetchStrategy: (Uri uri, FetchFailure failure) {
      if (failure != null) {
        //cannot get image
        return Future<FetchInstructions>.value(
            FetchInstructions.giveUp(uri: null));
      } else {
        return NetworkImageWithRetry.defaultFetchStrategy(uri, failure);
      }
    });

    return data;
  }

  static Future<File> getImageFileFromAssets(String path) async {
    final byteData = await rootBundle.load('assets/$path');

    final file = File('${(await getTemporaryDirectory()).path}/$path');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    return file;
  }

  static Future<File> writeToFile(ByteData data) async {
    final buffer = data.buffer;
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    var filePath =
        tempPath + '/file_01.tmp'; // file_01.tmp is dump file, can be anything
    return new File(filePath).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  static Map<String, int> fileNameCount;
  static Future<String> compressAndSaveFile(String fileName, File file) async {
    // comment ส่วนนี้ไว้ เพราะ สามารถ compress ที่ image picker ได้แล้ว
    // var result = await FlutterImageCompress.compressAndGetFile(
    //   file.absolute.path,
    //   file.path,
    //   // format: CompressFormat.png,
    //   quality: 30,
    // );

    if (fileNameCount == null) {
      fileNameCount = {};
    }
    if (fileNameCount[fileName] == null) {
      fileNameCount[fileName] = 0;
    }

    int count = fileNameCount[fileName];
    fileNameCount[fileName]++;

    fileName += '_' + count.toString();

    //save image
    final Directory dir = await getTemporaryDirectory();
    final String path = dir.path;
    final File localImage = await file.copy('$path/$fileName.jpg');

    return localImage.absolute.path;
  }
}
