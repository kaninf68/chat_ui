import 'dart:math' as math;

import 'package:intl/intl.dart';

class UString {
  static const _monthsTH = [
    "มกราคม",
    "กุมภาพันธ์",
    "มีนาคม",
    "เมษายน",
    "พฤษภาคม",
    "มิถุนายน",
    "กรกฎาคม",
    "สิงหาคม",
    "กันยายน",
    "ตุลาคม",
    "พฤศจิกายน",
    "ธันวาคม"
  ];
  static const _monthsShortTH = [
    "ม.ค.",
    "ก.พ.",
    "มี.ค.",
    "เม.ย.",
    "พ.ค.",
    "มิ.ย.",
    "ก.ค.",
    "ส.ค.",
    "ก.ย.",
    "ต.ค.",
    "พ.ย.",
    "ธ.ค.",
  ];

  static const _day = {
    'monday': 'จ.',
    'tuesday': 'อ.',
    'wednesday': 'พ.',
    'thursday': 'พฤ.',
    'friday': 'ศ.',
    'saturday': 'ส.',
    'sunday': 'อา.',
  };

  static String right(String s, int pre) {
    int startIndex = math.max(s.length - pre, 0);
    s = s.substring(startIndex, pre + startIndex);
    return s;
  }

  static String dateTimeShort(DateTime p) {
    if (p == null) {
      return '';
    }
    p = p.toLocal();
    return p.day.toString() +
        " " +
        _monthsShortTH[p.month - 1] +
        " " +
        (p.year + 543).toString().substring(2, 4) +
        ' - ' +
        p.hour.toString() +
        ':' +
        (p.minute < 10 ? '0' + p.minute.toString() : p.minute.toString());
  }
  // static String dateShort(DateTime p) {
  //   return p.day.toString() +
  //       " " +
  //       _monthsShortTH[p.month - 1] +
  //       " " +
  //       (p.year + 543).toString().substring(2, 4);
  // }

  static String timeShort(DateTime p) {
    if (p == null) {
      return '';
    }

    return (p.hour < 10 ? '0' + p.hour.toString() : p.hour.toString()) +
        ':' +
        (p.minute < 10 ? '0' + p.minute.toString() : p.minute.toString());
  }

  static String dateTime(DateTime p) {
    if (p == null) {
      return '';
    }

    return 'วันที่ ' +
        p.day.toString() +
        " " +
        _monthsTH[p.month - 1] +
        " " +
        (p.year + 543).toString() +
        ' เวลา ' +
        p.hour.toString() +
        ':' +
        (p.minute < 10 ? '0' + p.minute.toString() : p.minute.toString());
  }

  static String date(DateTime p) {
    if (p == null) {
      return '';
    }

    return 'วันที่ ' +
        p.day.toString() +
        " " +
        _monthsTH[p.month - 1] +
        " " +
        (p.year + 543).toString();
  }

  static String dateShort(DateTime p) {
    if (p == null) {
      return '';
    }

    return p.day.toString() +
        " " +
        _monthsShortTH[p.month - 1] +
        " " +
        (p.year + 543).toString().substring(2, 4);
  }

  static String dateTimeStamp(DateTime p) {
    if (p == null) {
      return '';
    }
    String _currentDay = DateFormat('EEEE').format(p).toLowerCase();
    if (p.day == DateTime.now().day) {
      return 'วันนี้';
    }
    if (p.day == DateTime.now().day - 1) {
      return 'เมื่อวาน';
    }
    return p.day.toString() +
        " " +
        _monthsShortTH[p.month - 1] +
        "(" +
        _day[_currentDay] +
        ')';
  }
}
