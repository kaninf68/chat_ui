import 'package:flutter/painting.dart';

class UStyle {
  static const ScaffoldBackgroundColor = Color(0xFFEFF1F1);
  static const AppbarColor = Color(0xFFF28530);

  static const BlueColor1 = Color(0xFF3472CD);

  static const OrangeColor1 = Color(0xFFEF7115);
  static const OrangeColor2 = Color(0xFFFF862E);
  static const OrangeColor3 = Color(0xFFFF872E);
  static const OrangeColor4 = Color(0xFFFFA00E);
  static const OrangeColor5 = Color(0xFFf7dbc5);

  static const BlackColor0 = Color(0xFF000000);
  static const BlackColor1 = Color(0xFF333333);
  static const BlackColor2 = Color(0xFF666666);
  static const BlackColor3 = Color(0xFF888888);
  static const BlackColor4 = Color(0xFF999999);
  static const GrayColor1 = Color(0xFF737373);
  static const GrayColor2 = Color(0xFF7D7D7D);
  static const GrayColor3 = Color(0xFF9B9B9B);
  static const GrayColor4 = Color(0xFFA4A4A4);
  static const GrayColor5 = Color(0xFFA9A9A9);
  static const GrayColor6 = Color(0xFFAAAAAA);
  static const GrayColor7 = Color(0xFFBBBBBB);
  static const GrayColor8 = Color(0xFFCCCCCC);
  static const GrayColor9 = Color(0xFFD5D5D5);
  static const GrayColor10 = Color(0xFFDDDDDD);
  static const GrayColor11 = Color(0xFFE0E0E0);
  static const GrayColor12 = Color(0xFFE3E3E3);
  static const GrayColor13 = Color(0xFFEEEEEE);
  static const GrayColor14 = Color(0xFFF1F1F1);
  static const GrayColor15 = Color(0xCCBDBDBD);

  static const GrayColor16 = Color(0xFFEBEBEB);
  static const GrayColor17 = Color(0xFF767676);
  static const GrayColor18 = Color(0xFFE6E6E6);
  static const GrayColor19 = Color(0xFF393939);

  static const GreenColor1 = Color(0xFF4caf50); // normal green

  static const RedColor1 = Color(0xFFE0026);
  static const RedColor2 = Color(0xFFF44336); // normal red

  static const WhiteColor1 = Color(0xFFFFFFFF); // normal white
  static const WhiteColor2 = Color(0x88FFFFFF); // normal white

  static const TextBlackColor1 = TextStyle(color: BlackColor1);
}
