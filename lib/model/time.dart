import 'string.dart';

class UTime {
  static String dateToString(DateTime date,
      [String time = '0', String minute = '0', String second = '0']) {
    if (date == null) {
      return '';
    }
    var d = date.day.toString();
    var m = date.month.toString();
    var y = date.year.toString();
    return y +
        "-" +
        UString.right("0" + m, 2) +
        "-" +
        UString.right("0" + d, 2) +
        'T' +
        UString.right('0' + time, 2) +
        ':' +
        UString.right('0' + minute, 2) +
        ':' +
        UString.right('0' + second, 2) +
        '+07:00';
  }

  static String dateToStringShort(DateTime date) {
    if (date == null) {
      return '';
    }
    var d = date.day.toString();
    var m = date.month.toString();
    var y = date.year.toString();
    return y +
        "-" +
        UString.right("0" + m, 2) +
        "-" +
        UString.right("0" + d, 2);
  }

  static String timeStamp(DateTime date) {
    if (date == null) {
      return '';
    }
    var d = date.day.toString();
    var m = date.month.toString();
    var hour = date.hour.toString();
    var minute = date.minute.toString();

    // DateTime today =
    //     DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    // DateTime timeStamp = DateTime(date.year, date.month, date.day);

    return UString.right("0" + hour, 2) + ":" + UString.right("0" + minute, 2);

    // if (today.isAtSameMomentAs(timeStamp)) {
    //   return UString.right("0" + hour, 2) +
    //       ":" +
    //       UString.right("0" + minute, 2);
    // } else {
    //   return UString.right("0" + d, 2) +
    //       "/" +
    //       UString.right("0" + m, 2) +
    //       " " +
    //       UString.right("0" + hour, 2) +
    //       ":" +
    //       UString.right("0" + minute, 2);
    // }
  }

  static DateTime stringUTCToTime(String date) {
    var d = DateTime.parse(date);
    return d.add(new Duration(hours: 7));
  }
}
