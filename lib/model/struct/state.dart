class MessagePosition {
  static const MESSAGE_POSITION_TOP = 'top';
  static const MESSAGE_POSITION_BOTTOM = 'bottom';
  static const MESSAGE_POSITION_MIDDLE = 'middle';
  static const MESSAGE_POSITION_ALONE = 'alone';
}
