class ChatMessageData {
  int id;
  int senderId;
  int type;
  String content;
  DateTime timestamp;
  String image;
  String sticker;

  ChatMessageData(Map data) {
    this.id = data['id'];
    this.senderId = data['userId'];
    this.type = data['type'];
    this.content = data['message'] ?? '';
    this.timestamp = DateTime.parse(data['createdAt']).toLocal();
    this.image = data['image'] ?? ''; // != null ? data['image'][0] : ''
    this.sticker = data['sticker'];
  }
}

class ChatMessageDataList {
  var list = new List<ChatMessageData>();

  parse(List items) {
    for (var item in items) {
      list.add(ChatMessageData(item));
    }
    return list;
  }
}
